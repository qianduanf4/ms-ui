const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const WebpackBar =  require("webpackbar");
let bannerPlugin  = new webpack.BannerPlugin({
  banner: 'yms-ui',
});


const isDev = process.env.NODE_ENV == "dev" ;
const entry = {
    "MSUI" : isDev ? "./src/index.js" : "./package/index.js"
}



const baseConfig = {
  entry:entry,
  output: {
    path: path.resolve(__dirname , 'package/lib'),
    filename: 'yms-ui.js',
    clean: true,
    chunkFilename: '[id].js',
    library: '[name]',
    libraryTarget: 'umd',
    umdNamedDefine: true,
    // globalObject: 'typeof self !== \'undefined\' ? self : this'

  },
  mode:  isDev  ? "development" : 'production',
  devtool: isDev ? 'eval-source-map' : "hidden-source-map",
  stats: 'errors-only',
  devServer: {
    hot: true,
    static: {
      directory: path.join(__dirname, 'public'),
    },
    compress: true,
    port: 9000,
    host: '0.0.0.0',
    client: {
      overlay: {
        errors: true,
        warnings: false,
      },
    },
  },
  module:{
    rules:[
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use:{
          loader: 'babel-loader',
          options: {
            presets: ["@babel/preset-env"],
            // 开启babel缓存
            // 第二次构建时，会读取之前的缓存
            // 用来缓存 loader的执行结果。之后的webpack 构建，将会尝试读取缓存，
            // 来避免在每次执行时，可能产生的、高性能消耗的 Babel 
            // 重新编译过程(recompilation process)。
            cacheDirectory: true,
          }
        }
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        'oneOf':[
          {
            test: /\.css$/,
            use: [
              isDev 
                ? 'vue-style-loader'
                : MiniCssExtractPlugin.loader,
              'css-loader'
            ]
          }, 
          {
            test: /\.scss$/,
            use: [{
              loader:MiniCssExtractPlugin.loader
            } , "css-loader","sass-loader"],
            // exclude: /(node_modules|bower_components)/,
          },
        ]
      },
    ]
  },
  plugins: [
   
    bannerPlugin,
    new MiniCssExtractPlugin({filename: "style/index.css"}),
    new VueLoaderPlugin(),
    new WebpackBar(),
    new HtmlWebpackPlugin({
      title:'webapck',
      template: "./public/index.html"
    })
  ],
    // 新增代码
  optimization: {

  },
}
if(!isDev) {
  /*
    1. 可以将node_modules中代码单独打包一个chunk最终输出
    2. 自动分析多入口chunk中，有没有公共的文件。如果有会打包成单独一个chunk
  */
  baseConfig.optimization.splitChunks  = {
    //这表明将选择哪些 chunk 进行优化。当提供一个字符串，有效值为 all，async 和 initial。
    //设置为 all 可能特别强大，因为这意味着 chunk 可以在异步和非异步 chunk 之间共享。
    chunks: "all",
  };
}

module.exports = baseConfig;