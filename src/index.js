import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from '../router'
import App from './App.vue'
import 'xe-utils'
import VXETable from 'vxe-table'
import 'vxe-table/lib/index.css'
import CommonVue from "../package/index";
Vue.use(CommonVue);
Vue.use(VXETable)
Vue.use(ElementUI)
new Vue({
  el: '#app',
  render: h => h(App),
  router,
})