import Vue from 'vue'
import Router from 'vue-router'
import AdaptSearch from "../src/example/adapt-search.vue";
import MultArea from "../src/example/mult-area.vue";
import CodeShow from "../src/example/code-show.vue";
import FlipCard from "../src/example/flip-card.vue";
import NumberScroll from "../src/example/number-scroll.vue";
import Carousel from "../src/example/carousel.vue";
Vue.use(Router)
export const constantRoutes = [
  {
    path: '/adapt-search',
    component: AdaptSearch,
  },
  {
    path: '/mult-area',
    component: MultArea,
  },
  {
    path: '/code-show',
    component: CodeShow,
  },
  {
    path: '/flip-card',
    component:FlipCard
  },
  {
    path: '/number-scroll',
    component:NumberScroll
  },
  {
    path: '/carousel',
    component:Carousel
  },

]


const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()



export default router