import AdaptBox from "./adapt-box.vue";
import AdaptBoxTable from "./adapt-box-table.vue"
AdaptBox.install = function(Vue){
  Vue.component(AdaptBox.name, AdaptBox );
}
AdaptBoxTable.install = function(Vue){
  Vue.component(AdaptBoxTable.name, AdaptBoxTable );
}
export  { AdaptBox , AdaptBoxTable } ;