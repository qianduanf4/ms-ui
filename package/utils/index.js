
/**
 * 去除所有空格
 * @param {*} value 
 * @param {*} symbleStr 
 * @returns 
 */
export const replaceAllBlank = function (value , symbleStr ) {
     return value.replace(/\s+/g , symbleStr);
}
/**
 * 替换回车换行
 * @param {*} value 
 * @param {*} symbleStr 
 * @returns 
 */
export const replaceAllLine = function(value ,symbleStr) {
    return value.replace(/[\r\n]/g, symbleStr);
}

export class StringChain {
   constructor(value){
     this.currentIndex = 0;
     this.value = value;
     this.preInfo = null;
   }
   call(fn , ...args ){
      let proxy = JSON.stringify(this.preInfo);
      this.preInfo ={
        currentIndex : this.currentIndex,
        value: this.value,
        preInfo: proxy
      }
      this.currentIndex ++ ;
      this.value = fn.call(null , this.value , ...args);
      return this;
   }
   getValue(){
       return this.value;
   }
}
