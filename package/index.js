import SvgIcon from "./svg-icon";
import  { AdaptBox , AdaptBoxTable }  from "./adapt-box";
import MultArea from "./mult-area";
import FlipCard from "./flip-card";
import CarouselItem from "./carousel-item";
import Carousel from "./carousel";
import "./theme-chalk/index.scss";

import BEMjs from "vue-bemjs";
const components = [ SvgIcon , AdaptBox  , AdaptBoxTable ,MultArea , FlipCard , CarouselItem , Carousel];
const install = function (Vue , options) {

  Vue.use(BEMjs , {namespace:"yms" , bemName:"bem"});
  components.forEach( component =>{
    Vue.component(component.name , component);
  })
  
}

/* istanbul ignore if */
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}

export default {
   version: require("./package.json").version,
   install,
   SvgIcon,
   AdaptBox,
   FlipCard,
   CarouselItem,
   Carousel
}