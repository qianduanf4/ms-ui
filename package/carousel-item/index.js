import NumberScroll from "./index.vue";

NumberScroll.install = function(Vue){
  Vue.component(NumberScroll.name , NumberScroll)
}

export default NumberScroll;