import FlipCard from "./index.vue";

FlipCard.install = function(Vue) {
  Vue.component(FlipCard.name , FlipCard);
}

export default FlipCard;