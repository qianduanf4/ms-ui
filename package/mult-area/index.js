import MultArea from "./index.vue";

MultArea.install = function(Vue){
  Vue.component(MultArea.name , MultArea);
}

export default MultArea;